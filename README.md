# SQLX zerolog

SQLXzerolog is a wrapper of the [sqlx](https://github.com/jmoiron/sqlx) 
with [zerolog](https://github.com/rs/zerolog) debug logging.

Logged base method without Tx or Stmp

