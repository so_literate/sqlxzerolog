module gitlab.com/so_literate/sqlxzerolog

go 1.13

require (
	github.com/jmoiron/sqlx v1.2.0
	github.com/rs/zerolog v1.15.0
	google.golang.org/appengine v1.6.5 // indirect
)
