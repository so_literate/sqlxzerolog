package sqlxzerolog

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

// BeginTxx begins a transaction and returns an *sqlx.Tx instead of an
// *sql.Tx.
//
// The provided context is used until the transaction is committed or rolled
// back. If the context is canceled, the sql package will roll back the
// transaction. Tx.Commit will return an error if the context provided to
// BeginxContext is canceled.
func (l *Logger) BeginTxx(ctx context.Context, opts *sql.TxOptions) (*sqlx.Tx, error) {
	l.Log.Debug().Str("method", "BeginTxx").Interface("opts", opts).Send()

	return l.DB.BeginTxx(ctx, opts)
}

// Beginx begins a transaction and returns an *sqlx.Tx instead of an *sql.Tx.
func (l *Logger) Beginx() (*sqlx.Tx, error) {
	l.Log.Debug().Str("method", "Beginx").Send()

	return l.DB.Beginx()
}

// Exec executes a query without returning any rows.
// The args are for any placeholder parameters in the query.
func (l *Logger) Exec(query string, args ...interface{}) (sql.Result, error) {
	l.Log.Debug().Str("method", "Exec").Str("query", query).Interface("args", args).Send()

	return l.DB.Exec(query, args...)
}

// Get using this DB.
// Any placeholder parameters are replaced with supplied args.
// An error is returned if the result set is empty.
func (l *Logger) Get(dest interface{}, query string, args ...interface{}) error {
	l.Log.Debug().Str("method", "Get").
		Interface("dest", dest).Str("query", query).Interface("args", args).Send()

	return l.DB.Get(dest, query, args...)
}

// GetContext using this DB.
// Any placeholder parameters are replaced with supplied args.
// An error is returned if the result set is empty.
func (l *Logger) GetContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	l.Log.Debug().Str("method", "GetContext").
		Interface("dest", dest).Str("query", query).Interface("args", args).Send()

	return l.DB.GetContext(ctx, dest, query, args...)
}

// NamedExec using this DB.
// Any named placeholder parameters are replaced with fields from arg.
func (l *Logger) NamedExec(query string, arg interface{}) (sql.Result, error) {
	l.Log.Debug().Str("method", "NamedExec").Str("query", query).Interface("arg", arg).Send()

	return l.DB.NamedExec(query, arg)
}

// NamedExecContext using this DB.
// Any named placeholder parameters are replaced with fields from arg.
func (l *Logger) NamedExecContext(ctx context.Context, query string, arg interface{}) (sql.Result, error) {
	l.Log.Debug().Str("method", "NamedExecContext").Str("query", query).Interface("arg", arg).Send()

	return l.DB.NamedExecContext(ctx, query, arg)
}

// NamedQuery using this DB.
// Any named placeholder parameters are replaced with fields from arg.
func (l *Logger) NamedQuery(query string, arg interface{}) (*sqlx.Rows, error) {
	l.Log.Debug().Str("method", "NamedQuery").Str("query", query).Interface("arg", arg).Send()

	return l.DB.NamedQuery(query, arg)
}

// NamedQueryContext using this DB.
// Any named placeholder parameters are replaced with fields from arg.
func (l *Logger) NamedQueryContext(ctx context.Context, query string, arg interface{}) (*sqlx.Rows, error) {
	l.Log.Debug().Str("method", "NamedQueryContext").Str("query", query).Interface("arg", arg).Send()

	return l.DB.NamedQueryContext(ctx, query, arg)
}

// QueryRow executes a query that is expected to return at most one row.
// QueryRow always returns a non-nil value. Errors are deferred until
// Row's Scan method is called.
// If the query selects no rows, the *Row's Scan will return ErrNoRows.
// Otherwise, the *Row's Scan scans the first selected row and discards
// the rest.
func (l *Logger) QueryRow(query string, args ...interface{}) *sql.Row {
	l.Log.Debug().Str("method", "QueryRow").Str("query", query).Interface("args", args).Send()

	return l.DB.QueryRow(query, args...)
}

// QueryRowx queries the database and returns an *sqlx.Row.
// Any placeholder parameters are replaced with supplied args.
func (l *Logger) QueryRowx(query string, args ...interface{}) *sqlx.Row {
	l.Log.Debug().Str("method", "QueryRowx").Str("query", query).Interface("args", args).Send()

	return l.DB.QueryRowx(query, args...)
}

// QueryRowxContext queries the database and returns an *sqlx.Row.
// Any placeholder parameters are replaced with supplied args.
func (l *Logger) QueryRowxContext(ctx context.Context, query string, args ...interface{}) *sqlx.Row {
	l.Log.Debug().Str("method", "QueryRowxContext").Str("query", query).Interface("args", args).Send()

	return l.DB.QueryRowxContext(ctx, query, args...)
}

// Queryx queries the database and returns an *sqlx.Rows.
// Any placeholder parameters are replaced with supplied args.
func (l *Logger) Queryx(query string, args ...interface{}) (*sqlx.Rows, error) {
	l.Log.Debug().Str("method", "Queryx").Str("query", query).Interface("args", args).Send()

	return l.DB.Queryx(query, args...)
}

// QueryxContext queries the database and returns an *sqlx.Rows.
// Any placeholder parameters are replaced with supplied args.
func (l *Logger) QueryxContext(ctx context.Context, query string, args ...interface{}) (*sqlx.Rows, error) {
	l.Log.Debug().Str("method", "QueryxContext").Str("query", query).Interface("args", args).Send()

	return l.DB.QueryxContext(ctx, query, args...)
}

// Select using this DB.
// Any placeholder parameters are replaced with supplied args.
func (l *Logger) Select(dest interface{}, query string, args ...interface{}) error {
	l.Log.Debug().Str("method", "Select").
		Interface("dest", dest).Str("query", query).Interface("args", args).Send()

	return l.DB.Select(dest, query, args...)
}

// SelectContext using this DB.
// Any placeholder parameters are replaced with supplied args.
func (l *Logger) SelectContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	l.Log.Debug().Str("method", "SelectContext").
		Interface("dest", dest).Str("query", query).Interface("args", args).Send()

	return l.DB.SelectContext(ctx, dest, query, args...)
}
