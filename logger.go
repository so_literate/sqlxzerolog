package sqlxzerolog

import (
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
)

// Logger is sqlx query logger
type Logger struct {
	DB  *sqlx.DB
	Log zerolog.Logger
}

// New creates new sqlx logger
func New(db *sqlx.DB, logger *zerolog.Logger) *Logger {
	log := logger.With().Bool("sqlx", true).Logger()

	return &Logger{
		DB:  db,
		Log: log,
	}
}
